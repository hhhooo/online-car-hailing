package com.xiaotan.passenger.service;

import com.xiaotan.projectcommon.dto.ResponseResult;
import com.xiaotan.projectcommon.dto.serviceverificationcode.request.SendCodeRequest;
import com.xiaotan.projectcommon.dto.serviceverificationcode.response.CodeResponse;

/**
 * @program: eureka-server
 * @description: 接口
 * @author: tgb
 * @create: 2020/11/13 10:25
 */
public interface IApiPassengerService {

	/***
	 *  发送验证码
	 * @param codeRequest
	 * @return
	 */
	ResponseResult sendCode(SendCodeRequest codeRequest);

	/***
	 * 校验验证码
	 * @param codeRequest
	 * @return
	 */
	ResponseResult checkCode(SendCodeRequest codeRequest);
}
