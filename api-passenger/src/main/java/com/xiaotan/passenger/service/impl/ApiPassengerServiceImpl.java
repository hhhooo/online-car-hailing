package com.xiaotan.passenger.service.impl;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.xiaotan.passenger.service.IApiPassengerService;
import com.xiaotan.projectcommon.dto.ResponseResult;
import com.xiaotan.projectcommon.dto.serviceverificationcode.request.SendCodeRequest;
import com.xiaotan.projectcommon.dto.serviceverificationcode.response.CodeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @program: eureka-server
 * @description: 实现
 * @author: tgb
 * @create: 2020/11/13 10:26
 */
@Service (value = "apiPassengerService")
public class ApiPassengerServiceImpl implements IApiPassengerService {

	@Autowired
	private RestTemplate restTemplate;

	private String codeUrl = "http://service-verification-code/verify-code/";

	@Override
	public ResponseResult sendCode(SendCodeRequest codeRequest) {

		String url = codeUrl + "produce-code";
		/***
		 * 	调用生成验证码服务
		 *
		 * 	postForEntity 发送请求，请求端需要加上注解@RequestBody
		 *
 		 */

		ResponseEntity<ResponseResult> responseEntity = restTemplate.postForEntity(url, codeRequest,
				ResponseResult.class,
				codeRequest.getPhoneNumber(),
				codeRequest.getCode(),
				codeRequest.getDeviceTerminal(),
				codeRequest.getIdentity());
		ResponseResult responseResult = responseEntity.getBody();
		JSONObject jsonObject = JSONUtil.parseObj(responseResult.getData());
		if (jsonObject == null) {
			return responseResult;
		}
		CodeResponse codeResponse = JSONUtil.toBean(jsonObject, CodeResponse.class);
		String code = codeResponse.getCode();

		// 调用短信服务发送验证码
		//TODO

		return responseResult;
	}

	@Override
	public ResponseResult checkCode(SendCodeRequest codeRequest) {
		String url = codeUrl + "check-code";
		/***
		 * 	调用生成验证码服务
		 *
		 * 	postForEntity 发送请求，请求端需要加上注解@RequestBody
		 *
		 */
		ResponseEntity<ResponseResult> responseEntity = restTemplate.postForEntity(url, codeRequest,
				ResponseResult.class,
				codeRequest.getPhoneNumber(),
				codeRequest.getCode(),
				codeRequest.getDeviceTerminal(),
				codeRequest.getIdentity());
		ResponseResult responseResult = responseEntity.getBody();
		return responseResult;
	}
}
