package com.xiaotan.passenger.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RetryRule;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @program: eureka-server
 * @description: bean注入配置类
 * @author: tgb
 * @create: 2020/11/13 10:11
 */
@Configuration
public class BeanFactoryConfig {


	/***
	 * 注入轮询策略
	 * @return
	 */
	@Bean
	public IRule loadBalancer() {
		return new RetryRule();
	}


	@LoadBalanced // 开启轮询策略注解
	@Bean
	public RestTemplate RequestTemplate() {
		return new RestTemplate();
	}

}
