package com.xiaotan.passenger.controller;

import com.xiaotan.passenger.service.IApiPassengerService;
import com.xiaotan.projectcommon.dto.ResponseResult;
import com.xiaotan.projectcommon.dto.serviceverificationcode.request.SendCodeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: eureka-server
 * @description: 乘客端
 * @author: tgb
 * @create: 2020/11/11 16:24
 */
@RestController
@RequestMapping("/api-passenger")
public class PassengerController {

	/***
	 * 		验证码功能
	 * 		乘客端 发送验证码
	 * 				调用验证码服务生成验证码功能, 调用短信发送验证码功能
	 *
	 * 		乘客端 登陆
	 * 				先校验验证码，验证码无误，再校验乘客信息
	 * 	 			验证成功跳转页面
	 */

	@Autowired
	IApiPassengerService apiPassengerService;

	@PostMapping ("/send-code")
	public ResponseResult sendCode(@Validated SendCodeRequest codeRequest) {
		return apiPassengerService.sendCode(codeRequest);
	}

	@PostMapping ("/check-code")
	public ResponseResult checkCode(@Validated SendCodeRequest codeRequest) {
		return apiPassengerService.checkCode(codeRequest);
	}

	@PostMapping ("/login")
	public ResponseResult login(@Validated SendCodeRequest codeRequest) {
		/***
		 *  TODO
		 *  登录功能
		 */
		return null;
	}

}
