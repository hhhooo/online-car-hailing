package com.xiaotan.serverzuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.xiaotan.projectcommon.constant.RedisKeyPrefixEnum;
import com.xiaotan.projectcommon.dto.ResponseResult;
import com.xiaotan.projectcommon.util.JWTUtil;
import com.xiaotan.projectcommon.util.JwtInfo;
import com.xiaotan.serverzuul.constant.ZuulConstant;
import com.xiaotan.serverzuul.constant.ZuulTimer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: eureka-server
 * @description: 网关过滤器
 * @author: tgb
 * @create: 2020/11/13 16:16
 */
@Component
public class AuthFilter extends ZuulFilter {

	@Autowired
	private Jedis jedis;

	@Override
	public String filterType() {
		/***
		 *  拦截类型
		 *
		 *  ERROR_TYPE = "error"; 在处理请求发生错误时被调用。
		 *
		 *  POST_TYPE = "post";	在route和error过滤器之后被调用。可以在响应添加标准HTTP Header、收集统计信息和指标，以及将响应发送给客户端等。
		 *
		 *  PRE_TYPE = "pre";  在请求被路由之前调用。Zuul请求微服务之前。比如请求身份验证，选择微服务实例，记录调试信息等。
		 *
		 *  ROUTE_TYPE = "route";  负责转发请求到微服务。原始请求在此构建，并使用Apache HttpClient或Netflix Ribbon发送原始请求。
		 */
		return FilterConstants.PRE_TYPE;
	}

	/***
	 *  数据越小，过滤越先执行
	 * @return
	 */
	@Override
	public int filterOrder() {
		return -1;
	}

	/***
	 * 过滤器是否生效
	 * @return
	 */
	@Override
	public boolean shouldFilter() {
		//获取上下文
		RequestContext requestContext = RequestContext.getCurrentContext();
		HttpServletRequest request = requestContext.getRequest();

		// 获取请求uri
		String requestURI = request.getRequestURI();

		// 设置不需要执行过滤器的请求
		String url = "/api-passenger";
		if (requestURI.indexOf(url) != -1) {
			return false;
		}

		return true;

	}

	@Override
	public Object run() throws ZuulException {
		System.out.println("开启拦截");
		//获取上下文
		RequestContext requestContext = RequestContext.getCurrentContext();
		HttpServletRequest request = requestContext.getRequest();
		String token = request.getHeader("Authorization");
		if (StringUtils.isEmpty(token)) {
			return new ResponseResult<>().setCode(ZuulConstant.ERROR_REQUEST.getCode()).setMessage(ZuulConstant.ERROR_REQUEST.getMessage());
		}

		JwtInfo jwtInfo = JWTUtil.parseToken(token);
		if (jwtInfo == null) {
			return new ResponseResult<>().setCode(ZuulConstant.ERROR_TAMPER.getCode()).setMessage(ZuulConstant.ERROR_TAMPER.getMessage());
		}
		String userIdToken = jwtInfo.getSubject();

		String redisToken = jedis.get(RedisKeyPrefixEnum.USER_TOKEN + userIdToken);

		if (StringUtils.isEmpty(redisToken)) {
			return new ResponseResult<>().setCode(ZuulConstant.ERROR_AUTHORITY.getCode()).setMessage(ZuulConstant.ERROR_AUTHORITY.getMessage());
		}

		// 鉴权成功，续约3分钟
		JWTUtil.createToken(userIdToken, ZuulTimer.TOKEN_RENEWAL_INTERVAL);

		/***
		 *  		// 不往下走，还走剩下的过滤器，但是不向后面的服务转发。
		 *         requestContext.setSendZuulResponse(false);
		 *         requestContext.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
		 *         requestContext.setResponseBody("auth fail");
		 */

		return null;
	}

}
