package com.xiaotan.serverzuul.constant;

/**
 * @program: eureka-server
 * @description: 时间配置
 * @author: tgb
 * @create: 2020/11/13 17:47
 */
public interface ZuulTimer {

	/***
	 * token 续约时间间隔 ,单位分钟, 每次续约3分钟
	 */
	Integer TOKEN_RENEWAL_INTERVAL = 3;

}
