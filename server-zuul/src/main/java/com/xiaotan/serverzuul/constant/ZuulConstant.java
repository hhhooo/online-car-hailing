package com.xiaotan.serverzuul.constant;

import lombok.Getter;

/**
 * @program: eureka-server
 * @description: 网关返回数据常量定义
 * @author: tgb
 * @create: 2020/11/13 17:02
 */
public enum ZuulConstant {

	ERROR_REQUEST(6001, "请求方式有误，没有token"),
	ERROR_AUTHORITY(6002, "没有权限，请登录验证"),
	ERROR_TAMPER(6003, "token篡改");




	@Getter
	private Integer code;

	@Getter
	private String message;

	ZuulConstant(Integer code, String message) {
		this.code = code;
		this.message = message;
	}}
