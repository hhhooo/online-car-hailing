package com.xiaotan.serverzuul.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;

/**
 * @program: eureka-server
 * @description: jedis
 * @author: tgb
 * @create: 2020/11/13 17:37
 */
@Configuration
public class JedisConfig {

	@Bean
	public Jedis useJedis() {
		Jedis jedis = new Jedis();
		return jedis;
	}

}
