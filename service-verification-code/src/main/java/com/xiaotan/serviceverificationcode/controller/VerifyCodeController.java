package com.xiaotan.serviceverificationcode.controller;

import com.xiaotan.projectcommon.dto.ResponseResult;
import com.xiaotan.projectcommon.dto.serviceverificationcode.request.SendCodeRequest;
import com.xiaotan.projectcommon.dto.serviceverificationcode.response.CodeResponse;
import com.xiaotan.serviceverificationcode.service.IVerifyCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: eureka-server
 * @description: 验证码服务，主要用于生成验证码、校验验证码
 * 		生成验证码 : 三重校验判断是否生成验证码
 * @author: tgb
 * @create: 2020/11/11 11:48
 */

@RestController
@RequestMapping("/verify-code")
public class VerifyCodeController {

	@Autowired
	private IVerifyCodeService service;
	/***
	 * 发送验证码
	 * @param codeRequest
	 * @return
	 */
	@PostMapping("/produce-code")
	public ResponseResult<CodeResponse> produceVerifyCode(@RequestBody SendCodeRequest codeRequest) {
		return service.produceVerifyCode(codeRequest);
	}

	/***
	 * 校验验证码
	 * @param codeRequest
	 * @return
	 */
	@PostMapping("/check-code")
	public ResponseResult<CodeResponse> checkCode(@RequestBody SendCodeRequest codeRequest) {
		return service.checkCode(codeRequest);
	}



}
