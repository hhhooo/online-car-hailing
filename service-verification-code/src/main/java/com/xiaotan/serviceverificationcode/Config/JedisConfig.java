package com.xiaotan.serviceverificationcode.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;

/**
 * @program: eureka-server
 * @description: redis
 * @author: tgb
 * @create: 2020/11/11 15:12
 */
@Configuration
public class JedisConfig {

	@Bean
	public Jedis useJedis() {
		Jedis jedis = new Jedis();
		return jedis;
	}

}
