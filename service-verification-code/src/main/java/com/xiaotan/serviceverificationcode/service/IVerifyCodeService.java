package com.xiaotan.serviceverificationcode.service;

import com.xiaotan.projectcommon.dto.ResponseResult;
import com.xiaotan.projectcommon.dto.serviceverificationcode.request.SendCodeRequest;
import com.xiaotan.projectcommon.dto.serviceverificationcode.response.CodeResponse;

/**
 * @program: eureka-server
 * @description: 验证码服务
 * @author: tgb
 * @create: 2020/11/11 14:14
 */
public interface IVerifyCodeService {

	/***
	 * 生成验证码服务
	 *  规定验证码在redis中key的存储方式为
	 *  RedisKeyPrefixEnum 前缀 + 电话号码
	 *
	 *
	 * @param codeRequest
	 * @return
	 */
	ResponseResult<CodeResponse> produceVerifyCode(SendCodeRequest codeRequest);

	/***
	 * 校验验证码是否正确
	 * @param codeRequest
	 * @return
	 */
	ResponseResult<CodeResponse> checkCode(SendCodeRequest codeRequest);
}
