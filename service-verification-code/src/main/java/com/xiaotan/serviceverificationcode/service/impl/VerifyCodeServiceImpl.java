package com.xiaotan.serviceverificationcode.service.impl;

import com.xiaotan.projectcommon.constant.VerifyCodeStatusEnum;
import com.xiaotan.projectcommon.constant.Identity;
import com.xiaotan.projectcommon.constant.RedisKeyPrefixEnum;
import com.xiaotan.projectcommon.dto.ResponseResult;
import com.xiaotan.projectcommon.dto.serviceverificationcode.request.SendCodeRequest;
import com.xiaotan.projectcommon.dto.serviceverificationcode.response.CodeResponse;
import com.xiaotan.projectcommon.util.VerifyCodeUtil;
import com.xiaotan.projectcommon.util.VerifyPhoneNumber;
import com.xiaotan.serviceverificationcode.service.IVerifyCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * @program: eureka-server
 * @description: impl
 * @author: tgb
 * @create: 2020/11/11 14:17
 */
@Service
public class VerifyCodeServiceImpl implements IVerifyCodeService {
	/***
	 * 乘客端同时调用验证码服务和短信服务
	 * 根据验证码服务的调用结果判断是否调用短信服务
	 */

	@Autowired
	private Jedis jedis;

	@Value("${local.zone}")
	private String zone;

	@Override
	public ResponseResult<CodeResponse> produceVerifyCode(SendCodeRequest codeRequest) {
		String phoneNumber = codeRequest.getPhoneNumber();
		// 校验手机号码是否为空  手机号码的校验使用validation注解，移到了乘客调用方
		/*if (phoneNumber == null) {
			return new ResponseResult<CodeResponse>().setCode(VerifyCodeStatusEnum.PHONENUMBER_EMPTY.getCode())
					.setMessage(VerifyCodeStatusEnum.PHONENUMBER_EMPTY.getValue());
		}

		// 校验手机号码格式是否正确
		Boolean checkNumber = VerifyPhoneNumber.verifyPhoneNumber(phoneNumber);
		if (! checkNumber) {
			return new ResponseResult<CodeResponse>().setCode(VerifyCodeStatusEnum.PHONENUMBER_ERROR.getCode())
					.setMessage(VerifyCodeStatusEnum.PHONENUMBER_ERROR.getValue());
		}*/

		String singleCodeKey = RedisKeyPrefixEnum.SINGLE_PHONE_NUMBER.getPREFIX_CODE() + phoneNumber;

		// 获取key的过期时间
		Long expireTime = jedis.ttl(singleCodeKey);

		if (expireTime != null && expireTime > 0) {
			// 转化时间
			String time = showExpireTime(expireTime);
			return new ResponseResult<CodeResponse>().setCode(VerifyCodeStatusEnum.FAIL.getCode())
					.setMessage("由于您的失败次数过多, 请在" + time + "时间后尝试");
		}

		// 判断redis当日验证码的次数， 分别做判断
		ResponseResult<CodeResponse> result = checkUser(codeRequest);
		return result;
	}

	@Override
	public ResponseResult<CodeResponse> checkCode(SendCodeRequest codeRequest) {
		String code = codeRequest.getCode();
		String redisKey = getRedisKey(codeRequest);
		String redisCode = jedis.get(redisKey);

		if (redisCode == null) {
			return new ResponseResult<CodeResponse>().setCode(VerifyCodeStatusEnum.VERIFICATION_IS_EXPIRE.getCode())
					.setMessage(VerifyCodeStatusEnum.VERIFICATION_IS_EXPIRE.getValue());
		}

		// 验证成功
		if (code!= null && code.equals(redisCode)) {
			// 验证码验证成功让原先的验证码立即失效
			jedis.expire(redisKey, 1);
			return new ResponseResult<CodeResponse>().setCode(VerifyCodeStatusEnum.SUCCESS.getCode())
					.setMessage(VerifyCodeStatusEnum.SUCCESS.getValue());
		}

		// 一分钟内相同验证码错误达3次，请1分钟后登录
		String redisCodeKey = RedisKeyPrefixEnum.SINGLE_CODE.getPREFIX_CODE() + code;
		Integer count = 0;
		if (jedis.get(redisCodeKey) != null) {
			count = Integer.parseInt(jedis.get(redisCodeKey));
		}
		if (count == 3) {
			return new ResponseResult<CodeResponse>().setCode(VerifyCodeStatusEnum.VERIFICATION_ONE_MIN_ERROR.getCode())
					.setMessage(VerifyCodeStatusEnum.VERIFICATION_ONE_MIN_ERROR.getValue());
		}

		// 记录单条验证码验证失败次数, 失效时间1分钟
		jedis.incr(redisCodeKey);
		jedis.expire(redisCodeKey, 60);

		// 记录单个手机号码条验证码验证失败次数, 失效时间
		String phoneNumber = codeRequest.getPhoneNumber();
		String singlePhoneKey = RedisKeyPrefixEnum.SINGLE_PHONE_NUMBER.getPREFIX_CODE() + phoneNumber;
		jedis.incr(singlePhoneKey);
		jedis.expire(singlePhoneKey, 10 * 60);
		return new ResponseResult<CodeResponse>().setCode(VerifyCodeStatusEnum.VERIFY_CODE_ERROR.getCode())
				.setMessage(VerifyCodeStatusEnum.VERIFY_CODE_ERROR.getValue());
	}


	/***
	 * 三档验证
	 * @param codeRequest
	 * @return
	 */
	public ResponseResult<CodeResponse> checkUser(SendCodeRequest codeRequest) {

		String phoneNumber = codeRequest.getPhoneNumber();
		// 单个手机号码一小时内获取验证码输入错误达3次，请10分钟后登录
		String singlePhoneKey = RedisKeyPrefixEnum.SINGLE_PHONE_NUMBER.getPREFIX_CODE() + phoneNumber;
		String count = jedis.get(singlePhoneKey);
		if (count != null && Integer.parseInt(count) == 3) {
			jedis.expire(singlePhoneKey, 10 * 60);
			return new ResponseResult<CodeResponse>().setCode(VerifyCodeStatusEnum.VERIFICATION_TEN_MIN_ERROR.getCode())
					.setMessage(VerifyCodeStatusEnum.VERIFICATION_TEN_MIN_ERROR.getValue());
		}

		//  单个手机号码一小时内验证码错误达5次，请24小时后登录
		if (count != null && Integer.parseInt(count) == 5) {
			jedis.expire(singlePhoneKey, 24 * 60 * 60);
			return new ResponseResult<CodeResponse>().setCode(VerifyCodeStatusEnum.VERIFICATION_TEN_MIN_ERROR.getCode())
					.setMessage(VerifyCodeStatusEnum.VERIFICATION_TEN_MIN_ERROR.getValue());
		}

		// 获取redis的key
		String redisKey = getRedisKey(codeRequest);

		String value = jedis.get(redisKey);

		// 如果redis中还存在短信验证码，则短信验证码还未过期，直接提示客户端，要求客户端输入验证码再次校验
		if (value != null) {
			return new ResponseResult<CodeResponse>().setCode(VerifyCodeStatusEnum.VERIFICATION_NOT_EXPIRE.getCode())
					.setMessage(VerifyCodeStatusEnum.VERIFICATION_NOT_EXPIRE.getValue());
		}

		// 生成验证码 存入redis, 有效时间3分种
		String code = VerifyCodeUtil.GenerateCode();
		jedis.set(redisKey, code);
		jedis.expire(redisKey, 3 * 60);
		return new ResponseResult<CodeResponse>().setCode(VerifyCodeStatusEnum.SUCCESS.getCode())
				.setMessage(VerifyCodeStatusEnum.SUCCESS.getValue()).setData(new CodeResponse().setCode(code));
	}

	private String getRedisKey(SendCodeRequest codeRequest) {
		String phoneNumber = codeRequest.getPhoneNumber();
		Integer identity = codeRequest.getIdentity();
		Integer deviceTerminal = codeRequest.getDeviceTerminal();
		String redisKey = "";
		if (identity.equals(Identity.driver)) {
			redisKey = RedisKeyPrefixEnum.DRIVER_LOGIN_CODE_KEY_PRE.getPREFIX_CODE() + phoneNumber;
		} else {
			redisKey = RedisKeyPrefixEnum.getPrefixCode(deviceTerminal);
		}
		return redisKey;
	}

	private String showExpireTime(Long expireTime) {
		Long nowExpireSecond = LocalDateTime.now().toEpochSecond(ZoneOffset.of("+" + zone)) + expireTime;
		LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(nowExpireSecond, 0, ZoneOffset.ofHours(Integer.parseInt(zone)));
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		String time = localDateTime.format(dateTimeFormatter);
		return time;
	}


}
