package com.xiaotan.projectcommon.constant;

import lombok.Getter;

/**
 * @program: eureka-server
 * @description: redis存储key
 * @author: tgb
 * @create: 2020/11/11 11:25
 */
public enum RedisKeyPrefixEnum {

	/**
	 * 乘客登录验证码 key前缀
	 */
	PASSENGER_LOGIN_CODE_KEY_PRE(3, "passenger_login_code_"),
	PASSENGER_LOGIN_TOKEN_APP_KEY_PRE(2, "passenger_login_token_app_"),
	PASSENGER_LOGIN_TOKEN_WEIXIN_KEY_PRE(1, "passenger_login_token_weixin_"),

	/**
	 * 司机登录验证码 key前缀
	 */
	DRIVER_LOGIN_CODE_KEY_PRE(4, "driver_login_code_"),

	/***
	 * 单条验证码登陆失败次数校验
	 */
	SINGLE_CODE(-1, "single_code_"),

	/***
	 * 单个手机验证码失败次数校验
	 */
	SINGLE_PHONE_NUMBER(-2, "single_phone_number_"),

	/***
	 * 用户登录token前缀
	 */
	USER_TOKEN(5, "user_token_");


	@Getter
	private String PREFIX_CODE;

	@Getter
	private Integer MARK;

	RedisKeyPrefixEnum(int MARK, String PREFIX_CODE) {
		this.PREFIX_CODE = PREFIX_CODE;
		this.MARK = MARK;
	}

	public static String getPrefixCode(Integer mark) {
		for (RedisKeyPrefixEnum data : RedisKeyPrefixEnum.values()) {
			if (data.getMARK() .equals(mark)) {
				return data.getPREFIX_CODE();
			}
		}
		return "";
	}
}
