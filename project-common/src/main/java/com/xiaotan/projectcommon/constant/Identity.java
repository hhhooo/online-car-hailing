package com.xiaotan.projectcommon.constant;

/**
 * @program: eureka-server
 * @description: 身份
 * @author: tgb
 * @create: 2020/11/11 12:04
 */
public interface Identity {

	/****
	 *  乘客
	 */
	Integer passenger = 1;


	/****
	 *  司机
	 */
	Integer driver = 2;

}
