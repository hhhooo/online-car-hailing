package com.xiaotan.projectcommon.constant;

import lombok.Getter;

/**
 * @program: eureka-server
 * @description: 设备终端
 * @author: tgb
 * @create: 2020/11/11 14:40
 */
public enum DeviceTerminalMarkEnum {

	WEIXIN(1, "微信端"),
	APP(2, "APP端"),
	WEB(3, "网页端"),
	IOS(4, "IOS端");

	@Getter
	private final int code;

	@Getter
	private final String value;

	DeviceTerminalMarkEnum(int code, String value) {
		this.code = code;
		this.value = value;
	}

}
