package com.xiaotan.projectcommon.dto.serviceverificationcode.request;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @program: eureka-server
 * @description: 发送验证码请求类
 * @author: tgb
 * @create: 2020/11/11 12:00
 */

@Data
@Accessors (chain = true)
public class SendCodeRequest {

	/***
	 *  身份
	 */
	@NotNull
	private Integer identity;

	/**
	 * 验证手机号，空和正确的手机号都能验证通过<br/>
	 * 正确的手机号由11位数字组成，第一位为1
	 * 第二位为 3、4、5、7、8
	 *
	 */
	@Pattern (regexp = "1[3|4|5|7|8][0-9]\\d{8}")
	private String phoneNumber;

	/**
	 * 设备终端d
	 */
	@NotNull
	private Integer deviceTerminal;

	/**
	 * 验证码
	 */
	private String code;

}
