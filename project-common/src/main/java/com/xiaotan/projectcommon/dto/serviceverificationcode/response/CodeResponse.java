package com.xiaotan.projectcommon.dto.serviceverificationcode.response;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @program: eureka-server
 * @description: 二维码发送响应
 * @author: tgb
 * @create: 2020/11/11 13:41
 */
@Data
@Accessors (chain = true)
public class CodeResponse {

	/****
	 *  验证码
	 */
	private String code;

}
