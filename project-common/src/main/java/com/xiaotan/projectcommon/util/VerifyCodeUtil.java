package com.xiaotan.projectcommon.util;

/**
 * @program: eureka-server
 * @description: 验证码
 * @author: tgb
 * @create: 2020/11/11 13:55
 */
public class VerifyCodeUtil {

	/***
	 * 生成验证码
	 * @return
	 */
	public static String GenerateCode() {
		// Math.pow 10的5次方
		return String.valueOf((int)((Math.random() * 9 + 1) * Math.pow(10,5)));
	}

	public static void main(String[] args) {
		System.out.println(Math.random() * 9 + 1);
		System.out.println(Math.pow(10,5));
	}

}
