package com.xiaotan.projectcommon.util;

/**
 * @program: eureka-server
 * @description: 封装jwt
 * @author: tgb
 * @create: 2020/11/11 11:20
 */
import lombok.Data;

@Data
public class JwtInfo{
	// 消息主体
	String subject;

	// 新的到期时间
	Long issueDate;
}
