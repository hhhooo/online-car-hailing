package com.xiaotan.projectcommon.util;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @program: eureka-server
 * @description: jwt
 * @author: tgb
 * @create: 2020/11/11 11:17
 */
public class JWTUtil {

	/**
	 * 密钥，仅服务端存储
	 */
	private static String secret = "ko346134h_we]rg3in_yip1!";

	/**
	 *
	 * @param subject
	 * @param issueDate 签发时间
	 * @return
	 */
	public static String createToken(String subject, Date issueDate) {
		String compactJws = Jwts.builder()
				.setSubject(subject)
				.setIssuedAt(issueDate)
				.signWith(io.jsonwebtoken.SignatureAlgorithm.HS512, secret)
				.compact();
		return compactJws;
	}

	/**
	 *
	 * @param subject
	 * @param timer 续签时间 单位分钟
	 * @return
	 */
	public static String createToken(String subject,Integer timer) {

		// 时间偏移续约
		DateTime dateTime = DateUtil.offsetMinute(new Date(), timer);

		String compactJws = Jwts.builder()
				.setSubject(subject)
                .setExpiration(dateTime)
				.signWith(io.jsonwebtoken.SignatureAlgorithm.HS512, secret)
				.compact();
		return compactJws;
	}

	/**
	 * 解密 jwt
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public static JwtInfo parseToken(String token) {
		try {
			Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
			if (claims != null){
				JwtInfo ji = new JwtInfo();
				ji.setSubject(claims.getSubject());
				ji.setIssueDate(claims.getIssuedAt().getTime());
				return ji;
			}
		}catch (ExpiredJwtException e){
			e.printStackTrace();
			System.out.println("jwt过期了");
		}

		return null;
	}
}
