package com.xiaotan.projectcommon.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @program: eureka-server
 * @description: 校验手机号码
 * @author: tgb
 * @create: 2020/11/11 14:25
 */
public class VerifyPhoneNumber {

	public static Pattern PATTERN_PHONE_NUMBER = Pattern.compile("^[1][3,4,5,7,8][0-9]{9}$");
	public static Boolean verifyPhoneNumber(String phoneNumber) {
		Matcher matcher = PATTERN_PHONE_NUMBER.matcher(phoneNumber);
		return matcher.matches();
	}
}
